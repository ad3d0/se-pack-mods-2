﻿using System;
using VRageMath;

namespace GSF.GSFPulseCannons
{
	public static class GSFWeaponManager
	{
//	Blaster Weapons
		public static PlasmaWeaponInfo FighterDualLightBlaster = new PlasmaWeaponInfo(5f, 50f, "MagazineFighterDualLightBlaster", 1, 1, 40);

		public static PlasmaWeaponInfo SmallPhotonTorpedo = new PlasmaWeaponInfo(20f, 475f, "MagazineSmallTorpedoPowerCellRed", 1, 1, 1);
		public static PlasmaWeaponInfo NovaTorpedo = new PlasmaWeaponInfo(20f, 875f, "MagazineNovaTorpedoPowerCellRed", 1, 1, 1);
		public static PlasmaWeaponInfo ThorMissile = new PlasmaWeaponInfo(10f, 575f, "MagazineThorMissilePowerCellOrange", 1, 1, 4);
		public static PlasmaWeaponInfo SmallThorMissile = new PlasmaWeaponInfo(10f, 275f, "MagazineSmallThorMissilePowerCellOrange", 1, 1, 1);

		
		public static PlasmaWeaponInfo SMLPLSMTLargeShot = new PlasmaWeaponInfo(6f, 100f, "MagazineSmallBlasterTurret", 1, 1, 4);
		public static PlasmaWeaponInfo MEDPLSMTLargeShot = new PlasmaWeaponInfo(12f, 200f, "MagazineMediumBlasterTurret", 1, 1, 4);
		public static PlasmaWeaponInfo LRGPLSMTLargeShot = new PlasmaWeaponInfo(24f, 300f, "MagazineLargeBlasterTurret", 1, 1, 6);
		public static PlasmaWeaponInfo XLrgPLSMBRDLargeShot = new PlasmaWeaponInfo(48f, 600f, "MagazineCitadelBlasterTurret", 1, 1, 4);



        //	Pulse Lasers


        //	Beams	

	}
}

